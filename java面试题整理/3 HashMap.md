---
title: 'HashMap底层、1.7与1.8区别、常见面试题'
date: 2020-04-27 13:11:29
tags: [java,HashMap]
published: true
hideInList: false
feature: 
isTop: false
---
谈谈hashMap的底层数据结构实现，以及JDK1.8后做了哪些改进、以及常见的面试题

<!--more-->

# 数据结构

**1.7**

> - 数组+链表
>
> - `Entry`节点是他的内部类，实现了`Map.ENTRY()`
> - 插入过程使用`头插法`：扩容时死循环

1.8

> - 数组+链表+红黑树
> - `Node`节点是他的内部类，实现了`Map.ENTRY()`
> - 使用`尾插法`：避免扩容时死循环

# 底层实现

从三个方面说：`put()`、 `get()`、 `resize()`，对比1.7版本与1.8版本。

## jdk7 版本

### put()

> 是有返回值的，
>
> 如果是key已存在，覆盖旧值，则返回旧值
>
> 如果新key不存在，则返回null

**源码**

> <img src="https://picgo-xzh.oss-cn-beijing.aliyuncs.com/picgo20200427113334.png" alt="image-20200427113334153" style="zoom:50%;" />

如图所示，整个`**流程**`可以大致分为四步处理

> 1. key为null的处理
> 2. 计算目标bucket的下标：即数组中的位置
> 3. 找到指定bucket，遍历Entry节点链表，若找到相同key相同的Entry节点，则做替换
> 4. 未没有找到目标Entry节点时，则新增一个Entry节点

`putForNllKey（value）`：key为null的处理说明

> 说明1：HashMap中，可以存在null的kv
>
> 说明2：key为null的，固定是放在table[0]的位置：即下标为0的bucket
>
> - 首先选择**table[0]**位置的链表
> - 对链表做遍历操作
> - 如果有节点的key为null，则将新value替换掉旧value
> - 如果没有key为null的节点，则新增一个key为null的Entry节点

`void addEntry(int hash, K key, V value, int bucketIndex)`

> 这是一个通用方法
>
> 给定四个参数，新增一个Entry节点，另外还担负了扩容职责。
>
> 如果哈希表中存放的**kv数量**超过了当前**阈值threshold**，且当前的bucket下标有链表存在，那么就进行**扩容resize**处理
>
> 扩容后，**重新计算hash**，得到新的bucket下标，然后使用**头插法**新增节点

### 2.1.2	resize()

**流程**

> 1. 插入kv时，发现容量不足
>
> 2. 保存旧的数组（old table）
>
> 3. 根据新容量（2倍）新建数组（new table）
>
> 4. 将old table上的kv 转移到 new table 中
>
>    > 1. 遍历旧数组的每个数据
>    > 2. 重新计算每个`旧kv`在新数组的位置（`不含新kv`）
>    > 3. 将旧数组上的每个`旧kv`,逐个转移到新数组中（头插法，`不含新kv`）
>
> 5. 新数组引用到HashMap的table属性上
>
> 6. 重新设置扩容阈值（threshold）
>
> 7. 扩容结束
>
>    > 此时table = 扩容后（2倍）+ 旧数据
>
> 8. 处理第一步要插入的kv
>
>    > 1. 计算新kv的hash值
>    > 2. 计算出新kv在table中的位置
>    > 3. 将数据插入到对应的位置

**源码**

<img src="https://picgo-xzh.oss-cn-beijing.aliyuncs.com/picgo20200427115545.png" alt="image-20200427115545105" style="zoom:50%;" />

**核心点1**：扩容后大小是扩容前的**2倍**

> 
>
> ```java
> oldCapacity = table.length;
> newCapacity = 2 * oldCapacity;
> ```

**核心点2**：**数据搬迁**：从旧table（即数组）迁到扩容后的新table

> 1. 先决策是否需要对每个Entry链表节点重新hash
>
> 2. 然后根据hash值计算得到bucket下标
>
> 3. 使用头插法做接点迁移

**核心点3**

> 

### 	get()

相对于put（）方法，get（）方法的实现就简单多了，主要分为两步

1. 通过key的hash值计算出在table中的下标
2. 遍历对应table的链表，逐个对比，得到结果。

### fail-fast策略

hashmap中有一个变量`modcount`，用于实现Hashmap中的快速失败

在对Map的做迭代(Iterator)操作时，会将**`modCount`**域变量赋值给**`expectedModCount`**局部变量。

在迭代过程中，用于做内容修改次数的一致性校验。

如果两个对比不一致，则会抛出异常**`ConcurrentModificationException`**

## jdk8 版本

### 与jdk1.7的核心差异

1. 数据结构不同，1.8为数组+链表+红黑树

2. resize时数据迁移的方式不同，1.8为尾插法

3. 链表节点的定义不同（内核没有变，不影响理解）：Entry-->Node类，都实现了Map.Entry()

   

### put()

与1.7类似，多了转换红黑树的步骤，头插法变尾插法，其他与1.7类似

<img src="https://picgo-xzh.oss-cn-beijing.aliyuncs.com/picgo20200427105750.png" alt="image-20200427105750336" style="zoom:50%;" />

**源码**

> <img src="https://picgo-xzh.oss-cn-beijing.aliyuncs.com/picgo20200427132313.png" alt="image-20200427132313825" style="zoom:50%;" />

### resize()

**流程**

> 1. 插入新kv，发现容量不足
>
> 2. 异常情况判断
>
>    > 1. 是否需要初始化
>    > 2. 若当前容量  > 最大值，则不扩容
>
> 3. 根据新容量（2倍）新建数组（new table）
>
> 4. 保存旧数组(old table)
>
> 5. 将旧数组上的数据转移到新数组中（old table -> new table）
>
>    > 1. 遍历数组的每个数据
>    >
>    > 2. 重新计算每个数据在新数组中的存储位置（`含新数据`）
>    >
>    >    > - 原位置  or  原位置+旧容量
>    >
>    > 3. 将旧数组上的每个主句逐个转移到新数组中（`含新数据`）
>    >
>    >    > 尾插法
>
> 6. 将新数组table引用到HashMap的table属性上
>
> 7. 重新设置扩容阈值（threshold）
>
> 8. 扩容结束

### get()

比较简单

1. 根据key计算出hash值，进一步计算得到table的下标，若bucket上为红黑树，则再进行红黑树查找，若不是红黑树，遍历链表

map家族的关系

<img src="https://picgo-xzh.oss-cn-beijing.aliyuncs.com/picgo20200427105258.png" alt="image-20200427105258224" style="zoom:50%;" />

# HashMap、HashTable对比

## 共同点

底层都是使用哈希表+链表的实现方式

## 不同点

- Hashtable线程安全，所有的操作都添加了Synchronized关键字修饰，效率低下

- 很少使用hashtable了，太旧了
- HashTable  的 kv 都不能为null，否则直接npe

# HashMap线程不安全的原因

## 数据覆盖问题

情景描述：

> 两个线程执行`put（）`操作时，可能导致数据覆盖，1.7、1.8都存在此问题
>
> a、b两个线程同时执行put操作，且两个key都指向同一个bucket，那么此时两个结点都会做头插法
>
> ```java
> public V put(K key, V value) {
>     ...
>     addEntry(hash, key, value, i);
> }
> 
> void addEntry(int hash, K key, V value, int bucketIndex) {
>     ...
>     createEntry(hash, key, value, bucketIndex);
> }
> 
> void createEntry(int hash, K key, V value, int bucketIndex) {
>     Entry<K,V> e = table[bucketIndex];
>     table[bucketIndex] = new Entry<>(hash, key, value, e);
>     size++;
> }
> 
> ```
>
> 看下最后的`createEntry()`方法，首先获取到了bucket上的头结点，然后再将新结点作为bucket的头部，并指向旧的头结点，完成一次头插法的操作。
>  当线程A和线程B都获取到了bucket的头结点后，若此时线程A的时间片用完，线程B将其新数据完成了头插法操作，此时轮到线程A操作，但这时线程A所据有的旧头结点已经过时了（并未包含线程B刚插入的新结点），线程A再做头插法操作，就会抹掉B刚刚新增的结点，导致数据丢失。
>
> 其实不光是`put()`操作，删除操作、修改操作，同样都会有覆盖问题。



## 扩容时导致死循环

1.7多线程会出现，因为扩容使用头插法，将原本的顺序做了翻转

1.8就没有了，因为用了尾插法。

# 如何规避HashMap的线程不安全

## 将Map转为包装类

**`Collections.SynchronizedMap()`**方法，实例代码：

```java
Map<String, Integer> testMap = new HashMap<>();
...
 // 转为线程安全的map
Map<String, Integer> map = Collections.synchronizedMap(testMap);
```

内部实现很简单，等同于Hashtable，只是对当前传入的map对象，新增对象锁（synchronized）

## 使用ConcurrentHashMap

# 链表转红黑树的阈值是8，而不是7或者20？

由于treenode（红黑树节点）的大小约是常规节点(HashMap的node节点？)的两倍，

因此我们仅在容器包含足够的节点以保证使用时，才才使用他们

> 这句话是什么意思呢？就是因为红黑树节点占用空间，不到万不得已不用他

当他们（红黑树）变得太小（由于移除或调整大小）时，会转换成普通节点

> 如果红黑树<=6退化成链表
>
> 如果链表长度>=8转变为红黑树
>
> 中间有个`差值7`可以有效防止链表和树的频繁转换
>
> 如果没有这个`差值7`的话，一个HashMap不听的插入删除，长度一会7一会8，就会发生频发的转变

容器中节点分布在hash桶中的频率符合泊松分布，桶的长度超过8的概率非常小，约百万分之1？

# 哈希表如何解决Hash冲突？

![image-20200427152811036](https://picgo-xzh.oss-cn-beijing.aliyuncs.com/picgo20200427152811.png)

# 为什么HashMap使用String、Integer这样的包装类做为key

![image-20200427152956728](https://picgo-xzh.oss-cn-beijing.aliyuncs.com/picgo20200427152956.png)

# 如果key为Object类型，需要如何处理?

重写`HashCode（）`、`equals()`

`HashCode（）`

> 计算数据的存储位置
>
> 实现不恰当，可能会导致严重的hash碰撞

`equals()`

> 用于比较是否存在相同key，用于新增或更新
>
> 保证键key在哈希表中的唯一性

# 为什么每次扩容是 2的幂次

1. 方便位运算
2. 均匀分布

# 如何计算在数组中的位置

用 hash 值对数组的长度取余(取模)

在 java 中是通过位运算来优化性能的，可以简单的理解成取模操作

通过 hash 函数，将字符串或者其他类型的 key 转化成数组的下标 index

给出一个长度为 8 的数组，当 key=001121 时

```java
index = hashCode("001121") % Array.length = 1420036703 % 8 = 7
```

# 扩容的条件

```java
// Capacity 即 HashMap 当前的长度
// LoadFactor 负载因子，默认是 0.75
HashMap.size() >= Capacity X LoadFactor
```

