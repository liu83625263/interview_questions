MDC：Mapped Diagnostic Context，映射调试上下文

是 log4j 和 logback 提供一种方便在多线程下记录日志的功能

可以将 MDC 看成是一个与当前线程绑定的哈希表，MDC 中的内容可以被同一线程中执行的代码所访问，当前线程的子线程会继承父线程中的 MDCD 内容

MDC 是通过 ThteadLocal 实现的

MDC 的常用方法：MDC.put(key，value)，获取时直接MDC.get(key)

MDC.put(key，value)源码如下

```java
    public static void put(String key, String val) throws IllegalArgumentException {
        if (key == null) {
            throw new IllegalArgumentException("key parameter cannot be null");
        } else if (mdcAdapter == null) {
            throw new IllegalStateException("MDCAdapter cannot be null. See also http://www.slf4j.org/codes.html#null_MDCA");
        } else {
            mdcAdapter.put(key, val);
        }
    }
```

调用了mdcAdapter.put()方法，MDCAdapter是一个接口，看下