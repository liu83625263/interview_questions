---
title: '谈谈线程的通信与协作：sleep、wait、notify、yield、join的区别'
date: 2020-04-23 15:34:30
tags: [java,线程]
published: true
hideInList: false
feature: 
isTop: false
---
谈谈线程的通信与协作：sleep、wait、notify、yield、join的区别

<!--more-->

# 

线程间通过共享数据来实现通信，即:多个线程主动读取一个共享数据。

举例：共享资源 int a；A、B两个线程轮序a的值，来确定自己做什么，浪费了cpu资源

# 靠共享变量来做控制

## wait/notify机制（配合Synchronized）

### 适用范围

Synchronized代码中才能使用，因为这几个都是obj的方法

即**等待通知机制**，主要由**object**类中的以下三个方法保证：wait、notify、notifyAll

这三个方法**不是Thread**中的方法，而**是Object**中的方法。原因是：每个对象都有monitor（锁），所以让当前某个线程等待某个对象锁，应该通过这个对象来操作，而不是让当前的线程来操作，因为当前线程可能会等待多个线程的锁。

### **sleep**

> **Thread**的方法
>
> 让出cpu资源，不让锁
>
> 当前线程进入阻塞，并释放cpu资源，无优先级、无法被**notify**唤醒

### **yield**

> **Thread**的方法
>
> 让出cpu时间，类似sleep，但是无法指定时长，让同优先级的线程有优先执行的机会，调用**yield**只是一个建议：告诉线程调度器，我好了，让相同优先级的线程使用用cpu吧。如果所有正在等待的线程都是低优先级的，则高线程继续执行

### **join**

> **Thread**的方法
>
> 不让锁
>
> 当前运行的线程调用另一个线程的join方法，等对方执行完之后再执行自己

wait、notify、notifyall都是obj的方法，必须用在synchronized中

### **wait**

> 让出cpu和锁，等待notify唤醒

### notify

> **随机唤醒**一个正在等待这个对象锁的线程

### notifyall

> **唤醒全部**正在等待这个对象锁的线程



wait()，notify()及notifyAll()只能在synchronized语句中使用，但是如果使用的是ReenTrantLock实现同步，该如何达到这三个方法的效果呢？解决方法是使用ReenTrantLock.newCondition()获取一个Condition类对象，然后Condition的await()，signal()以及signalAll()分别对应上面的三个方法。

## 2、Condition（配合Lock）

在jdk1.5中出现的，用于**替代**obj的wait、notify。实现线程间的协作，依赖于Lock，能够更安全高效的实现线程间协作。

Condition是个**接口**，基本方法就是**await（）、signal()**方法。

**使用范围：**

> **lock.lock()**和**lock.unlock**之间

Condition依赖于Lock接口

> 生成一个Condition的基本代码是：**lock.newCondition();**

**与Object对应方法如下**

> - **Conditon中的await()对应Object的wait()；**
> - **Condition中的signal()对应Object的notify()；**
> - **Condition中的signalAll()对应Object的notifyAll()。**

**多个线程（Thread）可以竞争同一把锁，一把锁也可以关联多个Condition**

> ![image-20200423141733716](https://picgo-xzh.oss-cn-beijing.aliyuncs.com/picgo20200423141733.png)

## 3、volatile

被volatile修饰的共享变量，具有以下两种特性

1. 禁止指令重排
2. **保证了不同线程对该变量操作的内存可见性**

volatile修饰的变量值，直接存在主内存中，保证所修饰的变量对于多个线程可见性，只要他被修改，其他线程读到的一定是最新的值。

> 对于自增操作不是原子性的，多个线程对一个volatile的变量进行++操作，结果不准。
>
> 想要原子自增需要用到下面的操作

## 4、利用AtomicInteger

底层实现是**Unsafe类**的**CAS操作**

> **在高并发情况下，LongAdder(累加器)比AtomicLong原子操作效率更高**
>
> LongAdder累加器是java8新加入的

# 信号量同步

信号量同步是指在不同的线程之间，通过传递同步信号量来协调线程执行的先后次序。这里重点分析其中2个

## **CountDownLatch**

允许一个或多个线程等待其他线程完成操作,本质上是一个**AQS计数器**，通过AQS来实现线程的等待与唤醒

> **需求：**解析一个文件下多个txt文件数据，可以用多线程解析，等到所有数据解析完毕后再进行其他操作

**代码实现：**

```java
public class CountDownLatchTest {
	// 等待几个点完成,就传入几
	private static CountDownLatch countDownLatch = new CountDownLatch(2);
  
	public static void main(String[] args) throws InterruptedException {
		Thread thread1 = new Thread(() -> {
			countDownLatch.countDown();
			System.out.println(Thread.currentThread().getName()+"gogo");
		});
		Thread thread2 = new Thread(() -> {
			countDownLatch.countDown();
			System.out.println(Thread.currentThread().getName()+"gogo");
		});
		thread2.start();
		thread1.start();

		countDownLatch.await();
		System.out.println("over");
	}

}
```



## Semaphore

> 场景：
>
> 车站案件：三个窗口在安检，哪个空闲了，就安排非空闲窗口的人去空闲的

## CyclicBarrier

循环栅栏

> 场景：
>
> 车站安检，3个人同时进去，3个人都安检完了，再放下一批三个人进去安检



# 三、PipedInputStream

用流在两个线程间通信，但是**java中的Stream是单向**的，需要在两个线程中分别建立input和output，性能差

# 四、BlockingQueue（阻塞队列）

BlockingQueue有一个特征：

当生产者线程试图向BlockingQueue中放入元素的时候，如果该队列已经满了，则该线程被阻塞;

当消费者试图从BlockingQueue中取出元素的时候，如果该队列已空，则线程被阻塞。

程序中两个线程交替向BlockingQueue放入、取出元素，就能很好的控制线程的通讯

