## 线程池的execute方法和submit方法有什么区别？

章内容很聚焦，但干货十足。不注意的话你可能会落入陷阱。

concurrent包里的`ExecutorService`，是一个接口，继承的是`Executor`，而`Executor`里只有一个方法。

```
publicinterface Executor {
    void execute(Runnable command);
}
```

这就是execute方法，接受一个runnable，然后返回为空。也就是说，它接受任务之后，就静悄悄异步去运行了。

我们再来看submit方法。区别就是submit方法，会返回一个Future对象。显然它是比execute方法多了一些内容的。

```
<T> Future<T> submit(Callable<T> task);
<T> Future<T> submit(Runnable task, T result);
Future<?> submit(Runnable task);
```

## 问题

我们尝试运行以下代码：

```
ExecutorService service = Executors.newFixedThreadPool(1);
Runnable r = () -> System.out.println(1 / 0);
service.submit(r);
service.shutdown();
```

程序静悄悄的什么都没有输出，异常没有，日志也没有，我们的错误直接被吞掉了。

把`submit`方法换成`execute`方法，可以看到异常能够正常输出。为了避免抄袭，我还是输出一些自定义的堆栈吧。

```
Exception in thread "pool-1-thread-1" java.lang.ArithmeticException: / by zero
	at com.github.xjjdog.pool.AAA.lambda$main$0(AAA.java:13)
	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1149)
	at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)
	at java.lang.Thread.run(Thread.java:748)
```

但它也仅仅是输出而已，我们无法使用`logback`之类的日志框架对其进行记录，因为它这个打印动作我们是不可控的。

## 解决方法

首先看下`submit` 方式的解决方法。通过返回的Future，执行它的get方法，即可获取完成的错误堆栈。

```
ExecutorService service = Executors.newFixedThreadPool(1);
Runnable r = () -> System.out.println(1 / 0);
Future f = service.submit(r);
f.get();
service.shutdown();
```

下面是输出结果。

```
Exception in thread "main" java.util.concurrent.ExecutionException: java.lang.ArithmeticException: / by zero
	at java.util.concurrent.FutureTask.report(FutureTask.java:122)
	at java.util.concurrent.FutureTask.get(FutureTask.java:192)
	at com.github.xjjdog.pool.AAA.main(AAA.java:20)
Caused by: java.lang.ArithmeticException: / by zero
	at com.github.xjjdog.pool.AAA.lambda$main$0(AAA.java:16)
	at java.util.concurrent.Executors$RunnableAdapter.call(Executors.java:511)
	at java.util.concurrent.FutureTask.run(FutureTask.java:266)
	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1149)
	at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)
	at java.lang.Thread.run(Thread.java:748)
```

但我们平常情况下，使用Future的时候并不多，因为它会阻塞我们的请求。

你可能怀疑不调用get，我们的代码没有运行，其实不是的。把runnable改成如下代码，不调用get方法，发现程序只输出了一个`a`。

```
Runnable r = () -> {
    System.out.println("a");
    System.out.println(1 / 0);
};
```

**真是让人恼火啊，想要抛出异常，还是使用execute方便一些。**

但我们上面说到，execute的方式，错误也是无法捕捉。其实我们可以曲线救国的绕一下去解决。解决方式就是使用`ThreadFactory`，实现它的`UncaughtExceptionHandler`。具体代码如下：

```
ThreadFactory factory = r->{
    Thread thread = Executors.defaultThreadFactory().newThread(r);
    thread.setUncaughtExceptionHandler( (t,e) -> {
        System.out.println(t + "" + e);
        e.printStackTrace();//example
    });
    return thread ;
};

ExecutorService service = Executors.newFixedThreadPool(1,factory);
Runnable r = () -> {
    System.out.println("a");
    System.out.println(1 / 0);
};

service.execute(r);
service.shutdown();
```

运行之后，能够看到我们的自定义异常捕获。

```
a
Thread[pool-1-thread-1,5,main]java.lang.ArithmeticException: / by zero
```

## End

Java线程池对于异常处理的这些默认行为，以及差别，我是特别抵触的。可以说两种默认行为都很low，我们还需要处理很多动作，才能捕捉到合适的异常。

多线程编程本来就难，又搞出这么两套东西来。找个日志吧，习惯性的往项目的error日志里去找，并没有。真是苦了开发同学。