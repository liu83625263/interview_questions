---
title: 'Reentrantlock的实现原理、以及公平锁和非公平锁的区别'
date: 2020-04-24 11:56:55
tags: [java,线程]
published: true
hideInList: false
feature: 
isTop: false
---
Reentrantlock的实现原理、以及公平锁和非公平锁的区别

<!-- more -->

# Reentrantlock 结构

<img src="https://picgo-xzh.oss-cn-beijing.aliyuncs.com/picgo20200424105918.png" alt="image-20200424105918105" style="zoom:50%;" />



首先`ReentrantLock`继承自父类`Lock`，然后有`3`个内部类，其中`Sync`内部类继承自`AQS`，另外的两个内部类继承自`Sync`，这两个类分别是用来**公平锁和非公平锁**的。

通过`Sync`重写的方法`tryAcquire`、`tryRelease`可以知道，**`ReentrantLock`实现的是`AQS`的独占模式，也就是独占锁，这个锁是悲观锁**。



# 锁类型

ReentrantLock分为公平锁、非公平锁。

可以通过构造方法指定具体类型，`默认非公`

```java
    //默认非公平锁
    public ReentrantLock() {
        sync = new NonfairSync();
    }
    
    //公平锁
    public ReentrantLock(boolean fair) {
        sync = fair ? new FairSync() : new NonfairSync();
    }

```

# 特性

与Synchronized的比较

|            | ReentrantLock                  | Synchronized     |
| ---------- | ------------------------------ | :--------------- |
| 锁实现机制 | 依赖AQS                        | 监视器模式       |
| 灵活性     | 支持响应中断、超时、尝试获取锁 | 不灵活           |
| 释放形式   | 必须显示调用unlock()进行解锁   | 自动释放监视器   |
| 锁类型     | 必须显示调用unlock()进行解锁   | 自动释放监视器   |
| 条件队列   | 可关联多个条件队列             | 关联一个条件队列 |
| 可重入性   | 可重入                         | 可重入           |

# 公平锁和非公平锁的区别

## 获取锁

**公平锁**：相当于排队买票，后来大人需要到队尾排队，不能插队

> 线程来了，先检查state是否被持有，如果被持有，当前线程就去同步队列排队

**非公平锁**：就是抢占模式，每来一个人不回去管队列如何，直接尝试获取锁

> 线程来了，先用cas的方式修改state，修改成功就获得锁，修改失败就去同步队列排队



## 释放锁

两者的流程是一样的：

判断当前线程是否是获得锁的线程，由于是重入锁，需要将`state`减到0才认为完全释放

释放之后需要调用 `unparkSuccessor(h)` 来唤醒被挂起的线程。

## 非公平锁的实现原理

### lock方法获取锁

1. `lock`方法调用`cas`方法设置`state`的值，如果`state`等于期望值`0`(代表锁没有被占用)，那么就将`state更新为1`（代表该线程获取锁成功），然后执行`setExclusiveOwnerThread`方法直接将该线程设置成锁的持有者。
2. 如果`cas`设置`state`失败，即state不等于0，代表锁被占用了
3. 检查state的值，如果是0，说明刚好释放资源了，则继续尝试cas设置state的值
4. 设置成功获得了锁
5. 设置失败state失败，则检查占用锁的线程是不是自己
   1. 如果是，就将`state+1`(可重入)
   2. 如果不是，就进入到同步队列中

### tryRelease 锁的释放

1. 判断当前线程是不是锁的所有者，如果不是，抛出异常
2. 如果是：判断此次释放后`state`的值是否为`0`，如果是则代表锁没有重入，然后将锁的持有者设置为null，唤醒同步队列的后继节点，进行锁的获取
3. 如果第二步中的释放后`state`的值`不为0`，即重入了：锁还没有释放完，不唤醒同步队列

## 公平锁的实现原理

### lock 方法获取锁

1. 获取`state`的值，如果`为0`，表示没有被占用（但不代表同步队列没有线程在等待）执行`步骤2`；如果`state！=0`执行`步骤3`
2. 判断同步队列是否存在线程（节点），如果不存在则直接将锁的所有者设置成当前线程，更新state状态
3. 判断锁的所有者是不是当前线程，如果是则更新state的值；如果不是自己，则加入同步队列中

### tryRelease锁释放

与非公平锁的释放过程是一样的