---
title: 'Synchronized的实现原理、锁的存储、以及锁是如何升级的'
date: 2020-04-23 16:13:30
tags: [java,线程]
published: true
hideInList: false
feature: 
isTop: false
---
Synchronized的实现原理、锁的存储、以及锁是如何升级的

<!--more-->

# 实现原理

**结论：**无论是代码块，还是方法，都是通过持有对象的锁来实现同步的

# 什么是对象的锁？

对象在内存中存储的布局分为三个区域：

1. **对象头**（header）

   1. **mark word**

      > 存储对象自身的运行时数据，比如hashcode，gc分代年龄、***锁的相关信息***

   2. klass point（类型指针）

      > 指针指向对象的类元数据，vm通过这个指针来确定该对象是哪个类的实例

   3. 数组长度（如果对象是数组的话，则有这个，否则没有）

2. 实例数据（instance data）

   > 对象属性信息，包括父类的属性信息

3. 对其填充（padding）

   > vm要求对象字节必须是8的倍数，凑数用的

**markword中，存储的内容如下**

![image-20200423155921960](https://picgo-xzh.oss-cn-beijing.aliyuncs.com/picgo20200423155922.png)

# 锁升级流程

无状态-->偏向锁-->轻量级锁-->重量级锁

每个线程在准备获取共享资源时：

1. 检查**markword**里是不是存放自己的threadID，如果是，当前为**偏向锁**

2. 如果不是，锁升级为**轻量级锁**，这时，用**CAS**来执行切换，新的线程根据**markword**里现有的**threadId**，通知之前的线程暂停，之前线程将**markWord**置为空

3. 两个线程都把锁对象的**hashcode**复制一份到各自新建的用于存储锁的记录空间；

   接着开始cas操作：把锁对象的**markword**的内容修改为各自新建的记录空间的地址

4. 在上一步中修改成功获得资源，失败的开始自旋

5. 自旋的过程中，成功获得资源则整个状态依然是轻量级锁

   > 自旋中获得资源，说明之前获得资源的线程执行完了，并释放了资源

6. 自旋一致失败，进入重量级锁，此时，自旋的线程阻塞，等待之前线程执行完并唤醒自己

# monitor

**monitor的组成**

> 1. EntryList
> 2. Owner（会指向持有 Monitor对象的线程）
> 3. waitSet



1. 当多个线程访问被 `synchronized`的方法或代码块时，这些线程会被放入`EntryList`队列，此时处于 blocking 状态
2. 当一个线程获取到实例的 monitor 锁时，那么该线程就进入 running 状态，执行方法，monitor 的 Owner 指向当前线程，count+1表示被一个线程持有
3. running 状态的线程调用 wait 方法，那么当前线程释放 monitor，owner 为 null，count-1，进入 waiting 状态，并进去 WaitSet 队列，直到被 notify 唤醒