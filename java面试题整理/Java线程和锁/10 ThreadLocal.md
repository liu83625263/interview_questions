---
title: ThreadLocal内存泄漏是怎么回事
tags:
  - ThreadLocal
categories:
  - 程序员
toc: true
published: 'true'
hideInList: 'false'
feature: 'null'
isTop: 'false'
date: 2020-04-28 11:24:53
---

ThreadLocal

初衷是线程并发时，解决变量共享问题，但由于过度设计，比如弱引用和哈希碰撞，导致理解难度大，使用成本高，反而成为故障高发点。容易出现内存泄漏、脏数据、共享对象更新等问题

<!-- more -->

不能称之为本地线程，而应该是CopyValueIntoEveryThread



# 是什么？

ThreadLocal 是一个本地线程副本变量工具类。主要用于将私有线程和该线程存放的副本对象做一个映射，各个线程之间的变量互不干扰，在高并发场景下，可以实现无状态的调用，适用于各个线程不共享变量值的操作。

# 工作原理

每个线程的内部维护了一个ThreadLocalMap，他是一个Map（key，value）数据格式

**key是一个弱引用**，也就是ThreadLocal本身。

而**value（强引用）**存的是线程变量的值

**为什么key是弱引用**

> 分两种情况来讨论这个问题
>
> 1. key使用强引用：
>
>    这样会导致一个问题，引用的ThreadLocal的对象被回收了，但是ThreadLocalMap还持有ThreadLocal的强引用，如果没有手动删除，ThreadLocal不被回收，会导致内存泄漏
>
> 2. key使用弱引用：
>
>    这样的话，引用的ThreadLocal被回收了由于 ThreadLocalMap 持有 ThreadLocal 的弱引用，即使没有手动删除，ThreadLocal 也会被回收。
>
> 比较两种请，发现：由于 ThreadLocalMap 的生命周期跟 Thread 一样长，如果都没有手动删除对应 key，都会导致内存泄漏，但是使用弱引用可以多一层保障，弱引用 ThreadLocal 不会内存泄漏，对应的 value 在下一次 ThreadLocalMap 调用 set、get、remove 的时候被清除，算是最优的解决方案。

# 使用场景

多线程场景-例如线程池、Hibernate 的 session 获取场景。

可以总结为2类用途

1. 保存线程上下文信息、在任意需要的地方可以获取
2. 线程安全的，避免某些情况需要考虑线程安全必须同步带来的性能损失

# 三个重要方法

- `set（）`：如果没有set操作的ThreadLocal，容易引起脏数据
- `get()`：始终没有get操作的ThreadLocal对象是无意义的
- `remove（）`：如果没有remove，容易引起内存泄漏

# 副作用

主要会产生的问题是：

- 脏数据
- 内存泄漏

这两个问题通常是在线程池的线程中使用ThreadLocal引发的，因为线程池由**线程复用**和**内存常驻**两个特点

## 脏数据

线程复用会产生仓数据，由于线程池会重用Thread对象，那么与Thread绑定的类的静态属性ThreadLocal变量也会被重用。

如果实现的线程`run()`方法但不显式的调用`remove()`清理与线程相关的ThreadLocal信息，下一个线程也不调用`set()`社会初始值，就可能`get()`到重用的线程信息，包括ThreadLocal所关联的线程对象的value值。

**举个例子**

> 高并发场景下，线程池中的线程可能会读取到上一个线程缓存的用户信息

## 内存泄漏

源码注释中提示使用static关键字来修饰ThreadLocal。在此场景下，寄希望于ThreadLocal对象失去引用后，触发弱引用机制来回收Entry的value就不现实了。

如果不进行remove操作，那么这个线程执行完成之后，通过THreadLocal对象持有的对象是不会被释放的

## 解决办法

每次用完ThreadLocal之后，都及时调用remove清理