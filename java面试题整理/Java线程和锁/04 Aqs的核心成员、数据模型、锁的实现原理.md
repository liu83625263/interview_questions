AQS（AbstractQueuedSynchronizer）就是一个抽象的队列同步器，AQS定义了一套多线程访问共享资源的同步器框架，许多同步类的实现都依赖于他
<!--more-->

# 什么是AQS

AQS（AbstractQueuedSynchronizer）就是一个抽象的队列同步器，AQS定义了一套多线程访问共享资源的同步器框架，许多同步类的实现都依赖于他，比如下面四个

- ReentrantLock
- Semaphore
- CountDownLatch
- CyclicBarrier

**JUC包下所有的锁都是基于它来实现的**

# AQS提供的方法

抽象类，就会有方法需要子类去重写，有以下几个需要子类重写

这几个方法`获取、释放锁`时都会`尝试`去修改同步状态state的值，因为`尝试`所以不一定成功。

| 方法                                        | 作用                       |
| ------------------------------------------- | -------------------------- |
| protected boolean tryAcquire(int arg)       | 独占式`尝试`获取锁         |
| protected boolean tryRelease(int arg)       | 独占式`尝试`释放锁         |
| protected int tryAcquireShared(int arg)     | 共享式`尝试`获取锁         |
| protected boolean tryReleaseShared(int arg) | 共享式`尝试`释放锁         |
| protected boolean isHeldExclusively()       | 当前线程是否独占式的占用锁 |

实际上只需要弄明白`acquire（）`和`release（）`方法即可，其他的方法与这两个方法的实现几乎一模一样。

# 数据模型

![image-20200424102713581](https://picgo-xzh.oss-cn-beijing.aliyuncs.com/picgo20200424102713.png)

在AQS中定义了一个int类型的变量：`state`用它来表示`同步状态`：哪个线程成功对state变量进行了`加1`的操作，那么这个线程就持有了锁

AQS还定了一个`FIFO（先进先出）队列`，用来表示等待获取锁的线程

使用`state`来表示同步状态，使用内置的`同步队列`来完成获取资源线程的排队工作

> `同步对列`是一个`FIFO队列`，它是一个由`Node`（AQS里面的静态内部类）节点组成的双向链表，每一个线程在获取同步状态时，如果获取同步状态失败，就会将当前线程封装成一个Node，然后将其加入到同步队列中

## AQS的三个核心成员变量

```java
// 队头结点
private transient volatile Node Head;
// 队尾结点
private transient volatile Node tail;
// 代表共享资源
private volatile ins state;
```

-  共享资源：volatile int state(代表共享状态）
-  队头节点：head头节点
-  队尾节点：tail尾节点

head、tail、state三个变量都是volatile的，通过volatile来保证共享变量的可见性。

## State状态的变更是基于CAS实现的

主要有三个方法

-  getState()
-  setState()
-  compareAndSetState()

state状态通过volatile保证共享变量的可见性，再由CAS 对该同步状态进行原子操作，从而保证原子性和可见性。

```java
    protected final int getState() {
        return state;
    }

    protected final void setState(int newState) {
        state = newState;
    }
 
    protected final boolean compareAndSetState(int expect, int update) {
        return unsafe.compareAndSwapInt(this, stateOffset, expect, update);
    }
```



## AQS资源共享方式

两种：独占模式、共享模式

`独占锁`：独占模式下，其他线程试图获取该锁将无法取得成功，只有一个线程能执行，如ReentrantLock采用独占模式

> ReentrantLock还可以分为公平锁和非公平锁：
>
> -  公平锁：按照线程在队列中的排队顺序，先到者先拿到锁
> -  非公平锁：当线程要获取锁时，无视队列顺序直接去抢锁，谁抢到就是谁的

`共享锁`多个线程获取某个锁可能会获得成功，多个线程可同时执行，如：Semaphore、CountDownLatch。

# AQS的锁释放与获取原理

![image-20200424103517468](https://picgo-xzh.oss-cn-beijing.aliyuncs.com/picgo20200424103517.png)

## 线程获取流程

1. 线程A获取锁，state将0置为1，线程A占用

2.  在A没有释放锁期间，线程B也来获取锁，线程B获取state为1，表示线程被占用，线程B创建Node节点放入队尾(tail)，并且阻塞线B

3. 同理线程C获取state为1，表示线程被占用，线程C创建Node节点，放入队尾，且阻塞线程

## 线程释放锁流程：

1. 线程A执行完，将state从1置为0

2. 唤醒下一个Node B线程节点，然后再删除线程A节点

3. 线程B占用，获取state状态位，执行完后唤醒下一个节点 Node C,再删除线程B节点