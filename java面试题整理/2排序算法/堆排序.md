# 什么是堆排序

二叉堆的构建、删除、自我调整等基本操作正是实现堆排序的基础

**二叉堆的特性**

1. 最大堆的堆顶是整个堆中最大的元素
2. 最小堆的堆顶是整个堆中最小的元素

以最大堆为力，如果删除一个最大堆的堆顶（并不是完全删除，而是跟末尾的节点交换位置），经过自我调整，第 2 大的元素就会被交换上来，成为最大堆的新堆

<img src="https://picgo-xzh.oss-cn-beijing.aliyuncs.com/picgo20200519165756.png" alt="image-20200519165756414" style="zoom:25%;" />

如图所示，在删除值为 10 的堆顶节点后，经过调整，值为 9 的新节点就会顶替上来

如果继续删除值为 9 的堆顶节点，值为 8 的新节点就会顶替上来

。。。

由于二叉堆的这个特性，每一次删除旧堆顶，调整后的新堆顶都是大小仅次于旧堆顶的节点，那么只要反复杀出堆顶，反复调整二叉堆，所得到的集合就会成为一个有序集合，过程如下

1. 删除节点 9，节点 8 成为新堆顶

   <img src="https://picgo-xzh.oss-cn-beijing.aliyuncs.com/picgo20200519170120.png" alt="image-20200519170059968" style="zoom:50%;" />

2. 删除节点 8，节点 7 成为新堆顶

   <img src="https://picgo-xzh.oss-cn-beijing.aliyuncs.com/picgo20200519170142.png" alt="image-20200519170142796" style="zoom:50%;" />

3. 删除节点 7 ，节点 6 成为新堆顶

   <img src="https://picgo-xzh.oss-cn-beijing.aliyuncs.com/picgo20200519170253.png" alt="image-20200519170253018" style="zoom:50%;" />

4. 删除节点 6，节点 5 成为新堆顶

   <img src="https://picgo-xzh.oss-cn-beijing.aliyuncs.com/picgo20200519170336.png" alt="image-20200519170336138" style="zoom:50%;" />

5. 删除节点 5，节点 4 成为新堆顶
   <img src="https://picgo-xzh.oss-cn-beijing.aliyuncs.com/picgo20200519170343.png" alt="image-20200519170343901" style="zoom:50%;" />

6. 删除节点 4，节点 3 成为新堆顶

   <img src="https://picgo-xzh.oss-cn-beijing.aliyuncs.com/picgo20200519170358.png" style="zoom:50%;" />

7. 删除节点 3，节点 2 成为新堆顶

   <img src="https://picgo-xzh.oss-cn-beijing.aliyuncs.com/picgo20200519170418.png" alt="image-20200519170418459" style="zoom:50%;" />

到此为止，原本的最大二叉堆已经变成了一个从小到大的有序集合

二叉堆实际是存储在数组汇总，数组中的元素排列如下

<img src="https://picgo-xzh.oss-cn-beijing.aliyuncs.com/picgo20200519170505.png" alt="image-20200519170505723" style="zoom:50%;" />

由此可以归纳出堆排序算法的步骤

# 算法步骤

1. 把无序数组构建成二叉堆
   - 需要从小到大排序，则构建成最大堆
   - 需要从大到小排序，则构建成最小堆
2. 循环删除堆顶元素，替换到二叉堆的末尾，调整对产生新的堆顶

# 代码实现

```java
package com.d1mq.algo;

/**
 *
 * @author xzh
 * @date 2020/5/19 5:14 下午
 * @since v
 *
 */
public class 堆排序 {
  	public static void main(String[] args) {
		int[] arr = new int[1000000];
		Random r = new Random();
		for (int i =0;i<arr.length;i++){
			int num = r.nextInt();
			arr[i] = num;
		}

		//排序前记录时间
		long start = System.currentTimeMillis();
    heapSort(arr);
		long end = System.currentTimeMillis();
		System.out.println(end-start);
	}
	/**
	 * 下沉调整
	 * @param arr 待调整的堆
	 * @param parentIndex 要"下沉"的父节点
	 * @param length 对的有效大小
	 */
	static void downAdjust(int[] arr, int parentIndex, int length) {
		//temp 保存父节点值,用于最后的复制
		int temp = arr[parentIndex];

		int childIndex = 2 * parentIndex + 1;
		while (childIndex < length) {
			//如果有右孩子,且右孩子大于做孩子的值,则定位到右孩子
			if (childIndex + 1 < length && arr[childIndex + 1] > arr[childIndex]) {
				childIndex++;
			}
			//如果父节点大于任何一个孩子的值,则直接跳出
			if (temp >= arr[childIndex]) {
				break;
			}

			//无须真正交换,单向赋值即可
			arr[parentIndex] = arr[childIndex];
			parentIndex = childIndex;
			childIndex = 2 * childIndex + 1;
		}
		arr[parentIndex] = temp;
	}

	/**
	 * 堆升序
	 * @param arr 待调整的堆
	 */
	static void heapSort(int[] arr){
		// 1.把无序数组构建成最大堆
		for (int i = (arr.length-2)/2;i>=0;i--){
			downAdjust(arr,i,arr.length);
		}
		// 2.循环删除堆顶元素,移到集合尾部,调整堆产生新的堆顶
		for (int i=arr.length-1;i>0;i--){
			// 最后一个元素和第一个元素进行交换
			int temp = arr[i];
			arr[i]=arr[0];
			arr[0] =temp;
			// 下沉 调整最大堆
			downAdjust(arr,0,i);
		}
	}
}

```

# 复杂度分析

## 空间复杂度

o(1)，没有开辟额外的空间

## 时间复杂度

二叉堆的节点“下沉” 调整（downAdjust 方法）是堆排序算法的基础，这个调节操作本身的时间复杂度是 Ologn

回顾一下堆排序算法的步骤

1. 把无序数组构建成二叉堆

   > 这一步是时间复杂度是 On

2. 循环删除堆顶元素，并将该元素移到集合尾部，调整堆产生新的堆顶

   > 需要进行 n-1 次循环，每次循环调用一个 downAdjust 方法，∴计算规模是（n-1）*logn，即时间复杂度为 nlogn

# 堆排与快排对比

## 相同点

平均时间复杂度都是 logn，并且都是不稳定排序

## 不同点

快排的最坏时间复杂度是 n2

堆排的最坏稳定在 nlogn