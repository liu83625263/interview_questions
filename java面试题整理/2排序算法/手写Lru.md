```java
public class Lru {
   public static void main(String[] args) {
      Lru l = new Lru(5);
      l.put("1","1");
      l.put("2","1");
      l.put("3","1");
      l.put("4","1");
      l.put("5","1");
      l.put("6","1");
      System.out.println(l.get("1"));
      System.out.println(l.get("6"));
   }
   private Node head;
   private Node end;
   // 缓存存储的上限
   private int limit;
   // 真实存储数据
   private HashMap<String, Node> hashMap;

   public Lru(int limit) {
      this.limit = limit;
      hashMap = new HashMap<String, Node>();
   }

   public String get(String key) {
      Node node = hashMap.get(key);
      if (node == null) {
         return null;
      }
      refreshNode(node);
      return node.getValue();
   }
   public void put(String key,String value){
      Node node = hashMap.get(key);
      if (node==null){
         //key 不存在,则插入
         if (hashMap.size()>=limit){
            // 如果发现已经满了,需要将头节点移除
            String oldKey = removeNode(head);
            hashMap.remove(oldKey);
         }
         node = new Node(key,value);
         addNode(node);
         hashMap.put(key,node);
      }else {
         // key 已经存在了,刷新 kv
         node.setValue(value);
         refreshNode(node);
      }
   }

   /**
    * 刷新被访问的节点位置
    * @param node 被访问的节点
    */
   private void refreshNode(Node node) {
      // 如果访问的是尾节点,则无需移动节点
      if (node == end) {
         return;
      }
      // 移除节点
      removeNode(node);
      // 重新插入节点
      addNode(node);
   }

   /**
    * 插入节点
    * @param node
    */
   private void addNode(Node node) {
      if (end != null) {
         end.setNext(node);
         node.setPre(end);
         node.setNext(null);
      }
      end = node;
      if (head == null) {
         head = node;
      }
   }

   /**
    * 删除节点
    * @param node 要删除的节点
    * @return
    */
   private String removeNode(Node node) {
      if (node == head && node == end) {
         //移除唯一的节点
         head = null;
         end = null;
      } else if (node == end) {
         // 移除尾节点
         end = end.getPre();
         end.setNext(null);
      } else if (node == head) {
         //移除头节点
         head = head.getNext();
      } else {
         //移除中间节点
         node.getPre().setNext(node.getNext());
         node.getNext().setPre(node.getPre());
      }
      return node.getKey();
   }
}


class Node {
   private Node pre;
   private Node next;
   private String key;
   private String value;

   public Node getPre() {
      return pre;
   }

   public void setPre(Node pre) {
      this.pre = pre;
   }

   public Node getNext() {
      return next;
   }

   public void setNext(Node next) {
      this.next = next;
   }

   public String getKey() {
      return key;
   }

   public void setKey(String key) {
      this.key = key;
   }

   public String getValue() {
      return value;
   }

   public void setValue(String value) {
      this.value = value;
   }

   Node(String key, String value) {
      this.key = key;
      this.value = value;
   }
}
```

