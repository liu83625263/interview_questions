# 题目

给出一个整数，删除指定个数字后，剩下的数最小

**翻译成人话**

> <img src="https://picgo-xzh.oss-cn-beijing.aliyuncs.com/picgo20200521163239.png" alt="image-20200521163239329" style="zoom:33%;" />

# 思路

贪心算法相关

## 你的第一感觉是优先删除最大的数字？

> 不对，数字的大小固然重要，但是数字的位置更重要，哪怕高位只减少 1，对数值的影响也是非常大的

## **举个例子**

> 一个9 位整数 541270936，要求删除 1 个数字，让剩下的整数尽可能小
>
> 此时无论删除什么数，都会从 9 位变成 8 位
>
> 既然随便删都是 8 位，那么显然应该应该把高位的数字降低。这样对新整数的影响最大

## 如何把高位的数字降低呢？

很简单，把原证书的所有数字从左到右依次进行比较

如果发现index 的值大于 index+1 的值，那么删除 index 的值后，必然会是这个数的值降低

因为右边比他小的数顶替了他的位置

## 总结

1. 从左往右遍历，找到第一个 index 大于 index+1 ，把 index 删掉

2. 如果删除 k 个，那么就是重复 k 次第一步操作

# 代码实现

```java
public class 删去k个数字后的最小值 {
	public static void main(String[] args) {
		System.out.println(removeKDigits("123456",1));
		System.out.println(removeKDigits("123456",2));
		System.out.println(removeKDigits("123456",3));
		System.out.println(removeKDigits("123456",4));
	}
	/**
	 *
	 * @param num 原证书
	 * @param k 删除数量
	 * @return
	 */
	static String removeKDigits(String num, int k) {
		// 新整数的长度 = 原证书长度-
		int newLength = num.length() - k;
		// 创建一个栈,用于接收所有的数字
		char[] stack = new char[num.length()];
		int top = 0;
		for (int i = 0; i < num.length(); i++) {
			//遍历当前数字
			char c = num.charAt(i);
			// 当栈顶的数组大于遍历到的当前数字时,栈顶数字出栈(相当于删除数字
			while (top > 0 && stack[top - 1] > c && k > 0) {
				top -= 1;
				k -= 1;
			}
			// 遍历到的当前数字入栈
			stack[top++] = c;
		}
		// 找到栈中的第一个非0数字的位置,以此构建新的整数字符串
		int offset = 0;
		while (offset < newLength && stack[offset] == '0') {
			offset++;
		}
		return offset == newLength ? "0" : new String(stack, offset, newLength - offset);
	}
}
```

## 代码说明

上面代码非常巧妙的运用了栈的特性，在遍历原整数的数字时，让所有数字一个一个入栈，当某个数字需要删除时，让该数字出栈

下面以整数 541270936  ，k=3 为例

![image-20200521170212326](https://picgo-xzh.oss-cn-beijing.aliyuncs.com/picgo20200521170212.png)

![image-20200521170222615](https://picgo-xzh.oss-cn-beijing.aliyuncs.com/picgo20200521170222.png)