用栈来模拟一个队列，要求实现队列的两个基本操作：入队和出队

# **栈的特点**

> 出入元素都是在栈顶

# **队列的特点**

> 先入先出，队头和队尾

# **提示**

> 一个栈不行，需要使用两个栈来实现队列

# **思路**

有两个栈，AB

A 负责入队操作

B 负责出队操作

<img src="https://picgo-xzh.oss-cn-beijing.aliyuncs.com/picgo20200521154903.png" alt="image-20200521154903684" style="zoom:33%;" />

# **可是两个栈是独立的，如何关联起来呢**

入队时，每个新元素都被压入 A 中

<img src="https://picgo-xzh.oss-cn-beijing.aliyuncs.com/picgo20200521154943.png" alt="image-20200521154943484" style="zoom:33%;" />

出队时，应该是 1 出队的，如何做呢

让 A 中的所有元素都出栈，按照出栈的顺序压入 B

> 这样一来，A 的入栈顺序是 1、2、3，B 的入栈顺序是3、2、1
>
> 此时 A 空了

出队的时候，依次取 B 的栈顶就可以了

**这个时候又想入队了呢**

> 这个时候想入队的话，依然压入 A，此时出队操作依然是从 B 出

如果 B 空了，还想要出队，那么只需要把 a 的压入 b 中即可

# 代码实现

```java
public class 用栈实现队列 {
	public static void main(String[] args) {
		StackQueue stackQueue = new StackQueue();
		stackQueue.enQueue(1);
		stackQueue.enQueue(2);
		stackQueue.enQueue(3);
		stackQueue.enQueue(4);
		System.out.println(stackQueue.deQueue());
		System.out.println(stackQueue.deQueue());
		System.out.println(stackQueue.deQueue());
		System.out.println(stackQueue.deQueue());
		stackQueue.enQueue(5);
		System.out.println(stackQueue.deQueue());
		System.out.println(stackQueue.deQueue());
	}

}
class StackQueue{
	private Stack<Integer> A = new Stack<Integer>();
	private Stack<Integer> B = new Stack<Integer>();

	/**
	 * 入队操作
	 * @param element
	 */
	void enQueue(int element){
		A.push(element);
	}
	/**
	 * 出队操作
	 * @return
	 */
	Integer deQueue(){
		if (B.isEmpty()){
			if (A.isEmpty()){
				return null;
			}
			while (!A.isEmpty()){
				B.push(A.pop());
			}
		}
		return B.pop();
	}

}
```

