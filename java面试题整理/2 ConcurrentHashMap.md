---
title: '谈谈ConcurrentHashMap的底层实现，1.7与1.8的详细区别'
date: 2020-04-28 10:08:42
tags: [java]
published: true
hideInList: false
feature: 
isTop: false
---
谈谈ConcurrentHashMap的底层实现，1.7与1.8的详细区别

<!--more-->

# jdk 1.7 版本的实现原理

## 数据结构

数组+链表

segment数组  + HashEntry链表

<img src="https://picgo-xzh.oss-cn-beijing.aliyuncs.com/picgo20200427162505.png" alt="image-20200427162505718" style="zoom:50%;" />

- `segment`

  > 是ConcurrentHashMap的一个内部类，主要的组成如下：
  >
  > ```java
  > static final class Segment<K,V> extends ReentrantLock implements Serializable {
  >    private static final long serialVersionUID = 2249069246763182397L;
  >    // 和 HashMap 中的 HashEntry 作用一样，真正存放数据的桶 
  >    transient volatile HashEntry<K,V>[] table;
  >    transient int count;
  >    // 记得快速失败（fail—fast）么？
  >    transient int modCount; 
  >    // 大小    
  >    transient int threshold;
  >    // 负载因子    
  >    final float loadFactor;
  > }
  > ```
  >
  > 

- HashEntry

  > 跟HashMap差不多，不同的是：使用了`volatile`去修饰他的数据`value`还有下一个节点`next`

## 高并发的原因

采用了分段锁技术，其中`segment`继承自`ReentrantLock`。

不像Hashtable一样：无论put还是get都需要做同步处理。

理论上**支持Segment数组数量的线程并发**

每一个线程占用锁访问一个Segment时，不会影响到其他Segment

如果说容量大小是16，那么他的并发度就是16：允许16个线程同时操作16个Segment而且线程安全。

## put过程

1. 先定位到Segment，然后再进行`put（）`操作

   > 如果value 为空，则npe

2. 尝试获取锁

   > 1. 如果获取失败，说明有其他线程存在竞争，则利用`scanAndLockForPut()` 自旋获取锁。
   > 2. 如果重试的次数达到了`MAX_SCAN_RETRIES`，则改为阻塞锁，保证能获取成功

3. 将当前`Segmen数组`中的table通过key的hashcode定位到`HashEntry链表`

## get 过程

由于HashEntry中的value是Volatile修饰的，保证了每次获取时都是最新值

1. 将key通过hash定位到具体的Segment
2. 再通过一次Hash定位到具体的元素上

## 这个版本存在的问题

因为基本还是数组 +链表的形式，我们去查询的时候，还得遍历链表，会导致效率很低

这个跟jdk1.7中HashMap中存在的问题是一样的。

# jdk 1.8 版本的实现原理

抛弃了原有的Segment分段锁，采用 `CAS` + `synchronized`来保证并发安全性

跟HashMap类似，也把之前的HashEntry 改成Node，但是作用不变。

`value` 和`next`采用`volatile`修饰，保证可见性。

引入了红黑树，链表大于一定值时会转换（默认是8）

## put

大致分为以下几步

1. 根据key计算出hashcode

2. 判断是否需要进行初始化

3. 当前key定位出的node；如果为空：利用 CAS 写入数据；失败则自旋保证成功

4. 如果当前位置的 hashcode == moved ==-1 ，则需要进行扩容

5. 如果都不满足，则利用synchronized锁写入数据

6. 如果数量大于TREEIFY_THRESHOLD，则转换为红黑树

   

```java
    /** Implementation for put and putIfAbsent */
    final V putVal(K key, V value, boolean onlyIfAbsent) {
        if (key == null || value == null) throw new NullPointerException();
     【1】//根据key计算出hashcode
        int hash = spread(key.hashCode());
        int binCount = 0;
        for (Node<K,V>[] tab = table;;) {
            Node<K,V> f; int n, i, fh;
          【2】//判断是否需要进行初始化
            if (tab == null || (n = tab.length) == 0)
                tab = initTable();
            else if ((f = tabAt(tab, i = (n - 1) & hash)) == null) {
              【3】//当前key定位出的node；如果为空：利用 CAS 写入数据；失败则自旋保证成功
                if (casTabAt(tab, i, null,
                             new Node<K,V>(hash, key, value, null)))
                    break;                   // no lock when adding to empty bin
            }
          【4】//如果当前位置的 hashcode == moved ==-1 ，则需要进行扩容
            else if ((fh = f.hash) == MOVED)
                tab = helpTransfer(tab, f);
            else {
              【5】//如果都不满足，则利用synchronized锁写入数据
                V oldVal = null;
                synchronized (f) {
                    ....
                }
                if (binCount != 0) {
                  【6】//如果数量大于TREEIFY_THRESHOLD，则转换为红黑树
                    if (binCount >= TREEIFY_THRESHOLD)
                        treeifyBin(tab, i);
                    if (oldVal != null)
                        return oldVal;
                    break;
                }
            }
        }
        addCount(1L, binCount);
        return null;
    }
```

## get

1. 根据计算出的hashcode寻址，如果就在桶上那么直接返回
2. 如果是红黑树，就按照红黑树的方式获取值
3. 如果是链表，就遍历链表获取值

```java
public V get(Object key) {
        Node<K,V>[] tab; Node<K,V> e, p; int n, eh; K ek;
        int h = spread(key.hashCode());
        【1】//根据计算出的hashcode寻址，如果就在桶上那么直接返回
        if ((tab = table) != null && (n = tab.length) > 0 &&
            (e = tabAt(tab, (n - 1) & h)) != null) {
          【2】//如果是红黑树，就按照红黑树的方式获取值
            if ((eh = e.hash) == h) {
                if ((ek = e.key) == key || (ek != null && key.equals(ek)))
                    return e.val;
            }
          【3】//如果是链表，就遍历链表获取值
            else if (eh < 0)
                return (p = e.find(h, key)) != null ? p.val : null;
            while ((e = e.next) != null) {
                if (e.hash == h &&
                    ((ek = e.key) == key || (ek != null && key.equals(ek))))
                    return e.val;
            }
        }
        return null;
    }
```

